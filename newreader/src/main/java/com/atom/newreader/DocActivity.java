package com.atom.newreader;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;

import java.io.File;
import java.io.InputStream;
import java.lang.ref.WeakReference;

public class DocActivity extends AppCompatActivity {

    // 显示进度条对话框
    public static final int HANDLER_MESSAGE_SHOW_PROGRESS = 1;
    public static final int HANDLER_MESSAGE_SEND_READER_INSTANCE = 2;
    public static final int HANDLER_MESSAGE_SUCCESS = 3;

    private static class MyHandler extends Handler {
        private WeakReference<DocActivity> activityWeakReference;

        public MyHandler(DocActivity activity) {
            activityWeakReference = new WeakReference<DocActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            DocActivity activity = activityWeakReference.get();
            if (activity != null) {

            }
        }
    }

    private MyHandler mMyHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doc);
        mMyHandler = new MyHandler(this);

        // show progress
        Message msg = new Message();
        msg.what = HANDLER_MESSAGE_SHOW_PROGRESS;
        mMyHandler.handleMessage(msg);


    }

    public class FileReaderThread extends Thread {

        private Context mContext;

        public FileReaderThread(Context mContext){
            this.mContext = mContext;
        }

        public void run() {
            // show progress
            Message msg = new Message();
            msg.what = HANDLER_MESSAGE_SHOW_PROGRESS;
            mMyHandler.handleMessage(msg);

            try {
                InputStream inputStream = mContext.getAssets().open("一密软件使用说明书.doc");

                // doc
                DOCReader reader = new DOCReader(inputStream);

                // 把IReader实例传出
                Message mesReader = new Message();
                mesReader.obj = reader;
                mesReader.what = HANDLER_MESSAGE_SEND_READER_INSTANCE;
                mMyHandler.handleMessage(mesReader);

                msg.obj = reader.getModel();
                reader.dispose();
                msg.what = HANDLER_MESSAGE_SUCCESS;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
