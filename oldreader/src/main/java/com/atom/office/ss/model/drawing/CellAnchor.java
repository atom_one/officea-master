
package com.atom.office.ss.model.drawing;

/**
 * TODO: 文件注释

 * 日期:            2012-3-1

 *

 */
public class CellAnchor {
    public final static short ONECELLANCHOR = 0x0;

    public final static short TWOCELLANCHOR = 0x1;


    public CellAnchor(short type) {
        this.type = type;
    }

    public short getType() {
        return type;
    }

    public void setStart(AnchorPoint start) {
        this.start = start;
    }

    public AnchorPoint getStart() {
        return start;
    }

    public void setEnd(AnchorPoint end) {
        this.end = end;
    }

    public AnchorPoint getEnd() {
        return end;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getWidth() {
        return width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void dispose() {
        if (start != null) {
            start.dispose();
            start = null;
        }

        if (end != null) {
            end.dispose();
            end = null;
        }
    }

    private short type = TWOCELLANCHOR;

    protected AnchorPoint start;
    private AnchorPoint end;

    //
    private int width;
    private int height;

}
