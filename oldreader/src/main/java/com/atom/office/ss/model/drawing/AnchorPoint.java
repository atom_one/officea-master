
package com.atom.office.ss.model.drawing;

/**
 * TODO: 文件注释

 * 日期:            2012-2-29

 *

 */
public class AnchorPoint {

    public AnchorPoint() {
    }

    public AnchorPoint(short col, int row, int dx, int dy) {
        this.row = row;
        this.col = col;
        this.dx = dx;
        this.dy = dy;
    }

    /**
     * @param row
     */
    public void setRow(int row) {
        this.row = row;
    }

    /**
     * @return
     */
    public int getRow() {
        return row;
    }

    /**
     * @param col
     */
    public void setColumn(short col) {
        this.col = col;
    }

    /**
     * @return
     */
    public short getColumn() {
        return col;
    }

    /**
     * @param dx
     */
    public void setDX(int dx) {
        this.dx = dx;
    }

    /**
     * @return
     */
    public int getDX() {
        return dx;
    }

    public void setDY(int dy) {
        this.dy = dy;
    }

    /**
     * @return
     */
    public int getDY() {
        return dy;
    }

    public void dispose() {

    }

    //
    protected int row;
    //
    protected short col;
    //
    protected int dx;
    //
    protected int dy;
}
