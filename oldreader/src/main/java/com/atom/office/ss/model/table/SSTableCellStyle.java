
package com.atom.office.ss.model.table;

/**
 * TODO: 文件注释

 * 日期:            2013-4-18

 *

 */
public class SSTableCellStyle {
    public SSTableCellStyle(int fillColor) {
        this.fillColor = fillColor;
    }

    public int getFontColor() {
        return fontColor;
    }


    public void setFontColor(int fontColor) {
        this.fontColor = fontColor;
    }

    public Integer getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(int borderColor) {
        this.borderColor = borderColor;
    }

    public Integer getFillColor() {
        return fillColor;
    }

    public void setFillColor(int fillColor) {
        this.fillColor = fillColor;
    }

    private int fontColor = 0xFF000000;
    //left, right, top, bottom
    private Integer borderColor;
    //background
    private Integer fillColor;
}
