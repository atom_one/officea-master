
package com.atom.office.ss.other;

import com.atom.office.ss.model.CellRangeAddress;
import com.atom.office.ss.model.baseModel.Cell;

/**
 * TODO: 文件注释

 * 日期:            2012-6-4
 * <p>
 *


 */
public class ExpandedCellRangeAddress 
{
    public ExpandedCellRangeAddress(Cell expandedCell, int firstRow, int firstCol, int lastRow, int lastCol)
    {
        this.expandedCell = expandedCell;
        rangeAddr = new CellRangeAddress(firstRow, firstCol, lastRow, lastCol);
    }

    /**
     * 
     * @return
     */
    public CellRangeAddress getRangedAddress()
    {
        return rangeAddr;
    }
    
    /**
     * 
     * @return
     */
    public Cell getExpandedCell()
    {
        return expandedCell;
    }
    
    
    public void dispose()
    {
        rangeAddr = null;
        
        expandedCell = null;
        
    }
    private CellRangeAddress rangeAddr;
    
    private Cell expandedCell;
}
