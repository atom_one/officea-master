
package com.atom.office.ss.model.drawing;

import com.atom.office.simpletext.font.Font;
import com.atom.office.ss.model.style.Alignment;


/**
 * TODO: 文件注释

 * 日期:            2012-3-1

 *

 */
public class TextParagraph {
    public TextParagraph() {
        align = new Alignment();
    }

    /**
     * @param textRun
     */
    public void setTextRun(String textRun) {
        this.textRun = textRun;
    }

    /**
     * @return
     */
    public String getTextRun() {
        return textRun;
    }

    /**
     * @param fontIndex
     */
    public void setFont(Font font) {
        this.font = font;
    }

    /**
     * @return
     */
    public Font getFont() {
        return font;
    }

    /**
     * @param horizon
     */
    public void setHorizontalAlign(short horizon) {
        align.setHorizontalAlign(horizon);
    }

    /**
     * @return
     */
    public short getHorizontalAlign() {
        return align.getHorizontalAlign();
    }

    /**
     * @param vertical
     */
    public void setVerticalAlign(short vertical) {
        align.setVerticalAlign(vertical);
    }

    /**
     * @return
     */
    public short getVerticalAlign() {
        return align.getVerticalAlign();
    }

    public void dispose() {
        textRun = null;
        font = null;

        if (align != null) {
            align.dispose();
            align = null;
        }

    }

    private String textRun;
    private Font font;
    private Alignment align;
}
