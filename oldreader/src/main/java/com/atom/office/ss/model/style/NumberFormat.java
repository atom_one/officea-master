
package com.atom.office.ss.model.style;

/**
 * TODO: 文件注释

 * 日期:            2012-2-22

 *

 */
public class NumberFormat {

    public NumberFormat() {
        numFmtId = 0;
        formatCode = "General";
    }

    public NumberFormat(short numFmtId, String formatCode) {
        this.numFmtId = numFmtId;
        this.formatCode = formatCode;
    }

    public void setNumberFormatID(short id) {
        numFmtId = id;
    }

    /**
     * get Number Format Id
     *
     * @return
     */
    public short getNumberFormatID() {
        return numFmtId;
    }

    /**
     * @param formatCode
     */
    public void setFormatCode(String formatCode) {
        this.formatCode = formatCode;
    }

    /**
     * get Number Format Code
     *
     * @return
     */
    public String getFormatCode() {
        return formatCode;
    }


    /**
     *
     */
    public void dispose() {
        formatCode = null;
    }

    //Number Format Id
    private short numFmtId;
    //Number Format Code
    private String formatCode;
}
