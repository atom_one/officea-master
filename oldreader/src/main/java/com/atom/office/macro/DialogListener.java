
package com.atom.office.macro;

import com.atom.office.common.ICustomDialog;

/**
 * TODO: 文件注释

 * 日期:            2012-12-17

 *

 */
public interface DialogListener {
    //password dialog
    public static final byte DIALOGTYPE_PASSWORD = ICustomDialog.DIALOGTYPE_PASSWORD;
    //txt encode dialog
    public static final byte DIALOGTYPE_ENCODE = ICustomDialog.DIALOGTYPE_ENCODE;
    //loading dialog
    public static final byte DIALOGTYPE_LOADING = ICustomDialog.DIALOGTYPE_LOADING;
    //error dialog
    public static final byte DIALOGTYPE_ERROR = ICustomDialog.DIALOGTYPE_ERROR;
    //
    public static final byte DIALOGTYPE_FIND = ICustomDialog.DIALOGTYPE_FIND;


    /**
     * @param type dialog type
     */
    public void showDialog(byte type);

    /**
     * @param type
     */
    public void dismissDialog(byte type);
}
