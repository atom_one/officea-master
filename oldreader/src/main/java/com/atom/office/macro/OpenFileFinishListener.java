
package com.atom.office.macro;

/**
 * open file finish listener

 * <p>
 * 日期:            2012-9-10

 *

 */
public interface OpenFileFinishListener {
    /**
     * when open file finish after is callback this method.
     */
    public void openFileFinish();

}
