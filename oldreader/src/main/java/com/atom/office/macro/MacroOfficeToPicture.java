
package com.atom.office.macro;

import android.graphics.Bitmap;

import com.atom.office.common.IOfficeToPicture;

/**
 * generated picture

 * <p>
 * 日期:            2012-9-10

 *

 */
class MacroOfficeToPicture implements IOfficeToPicture {

    protected MacroOfficeToPicture(OfficeToPictureListener listener) {
        this.officeToPictureListener = listener;
    }

    /**
     * set mode type
     *
     * @param modeType
     */
    public void setModeType(byte modeType) {
        this.modeType = modeType;
    }

    /**
     *
     *
     */
    public byte getModeType() {
        return modeType;
    }

    /**
     * Get converter to of picture Bitmap instance, if the return is empty, is not generated picture
     *
     * @param componentWidth  engine component width
     * @param componentHeight engine component height
     * @return Bitmap instance
     */
    public Bitmap getBitmap(int componentWidth, int componentHeight) {
        if (officeToPictureListener != null) {
            return officeToPictureListener.getBitmap(componentWidth, componentHeight);
            //return officeToPictureListener.getBitmap(845, 480);
        }
        return null;
    }

    /**
     * picture generated, the callback method
     *
     * @param bitmap generated picture bitmap
     */
    public void callBack(Bitmap bitmap) {
        if (officeToPictureListener != null) {
            officeToPictureListener.callBack(bitmap);
        }
    }

    /**
     *
     *
     */
    public boolean isZoom() {
        return true;
    }

    /**
     *
     *
     */
    public void dispose() {
        officeToPictureListener = null;
    }

    //
    private OfficeToPictureListener officeToPictureListener;
    private byte modeType = VIEW_CHANGE_END;
}
