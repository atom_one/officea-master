
package com.atom.office.macro;

import android.graphics.Bitmap;

import com.atom.office.constant.MainConstant;
import com.atom.office.fc.ReaderThumbnail;

/**
 * get Thumbnail kit

 * <p>
 * 日期:            2013-2-25

 *

 */
public class ThumbnailKit {
    private static ThumbnailKit kit = new ThumbnailKit();

    /**
     *
     */
    public static ThumbnailKit instance() {
        return kit;
    }

    /**
     * @param filePath
     * @param width    thumbnail width
     * @param height   thumbnail height
     * @return
     */
    public Bitmap getPPTThumbnail(String filePath, int width, int height) {
        try {
            String lowerCase = filePath.toLowerCase();
            if (lowerCase.indexOf(".") > 0
                    && width > 0
                    && height > 0
                    && (lowerCase.endsWith(MainConstant.FILE_TYPE_PPT)
                    || lowerCase.endsWith(MainConstant.FILE_TYPE_POT))) {
                return ReaderThumbnail.instance().getThumbnailForPPT(filePath, width, height);
            }
        } catch (Exception e) {

        }

        return null;
    }

    /**
     * @param filePath
     * @return
     */
    public Bitmap getPPTXThumbnail(String filePath) {
        try {
            String lowerCase = filePath.toLowerCase();
            if (lowerCase.indexOf(".") > 0
                    && (lowerCase.endsWith(MainConstant.FILE_TYPE_PPTX)
                    || lowerCase.endsWith(MainConstant.FILE_TYPE_PPTM)
                    || lowerCase.endsWith(MainConstant.FILE_TYPE_POTX)
                    || lowerCase.endsWith(MainConstant.FILE_TYPE_POTM))) {
                return ReaderThumbnail.instance().getThumbnailForPPTX(filePath);
            }
        } catch (Exception e) {

        }

        return null;
    }

    /**
     * @param filePath
     * @param zoom     (0 < thumbnail zoom value <= MAXZOOM_THUMBNAIL )
     * @return
     * @see com.atom.office.macro.Application #MAXZOOM_THUMBNAIL
     */
    public Bitmap getPDFThumbnail(String filePath, int zoom) {
        try {

            String lowerCase = filePath.toLowerCase();
            if (lowerCase.indexOf(".") > 0
                    && lowerCase.endsWith(MainConstant.FILE_TYPE_PDF)
                    && zoom > 0 && zoom <= Application.MAXZOOM_THUMBNAIL) {
                return ReaderThumbnail.instance().getThumbnailForPDF(filePath, zoom / (float) MainConstant.STANDARD_RATE);
            }
        } catch (Exception e) {

        }
        return null;
    }
}
