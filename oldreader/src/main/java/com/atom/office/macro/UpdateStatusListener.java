
package com.atom.office.macro;

/**
 * update status listener

 * <p>
 * 日期:            2012-9-10

 *

 */
public interface UpdateStatusListener {
    //header or footer contains vector graph,so need update all pages
    public static final byte ALLPages = -1;

    /**
     * update status for UI, ex. changes in the current slide
     */
    public void updateStatus();

    /**
     * callback this method after zoom change
     */
    public void changeZoom();

    /**
     *
     */
    public void changePage();

    /**
     *
     */
    public void completeLayout();

    /**
     * @param views
     */
    public void updateViewImage(Integer[] views);

}
