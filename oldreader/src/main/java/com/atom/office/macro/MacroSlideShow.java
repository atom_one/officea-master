
package com.atom.office.macro;

import com.atom.office.common.ISlideShow;

/**
 * TODO: 文件注释

 * 日期:            2012-12-28

 *

 */
public class MacroSlideShow implements ISlideShow {
    /**
     *
     *
     */
    protected MacroSlideShow(SlideShowListener listener) {
        this.listener = listener;
    }

//    /**
//     * 
//     * @param actionType
//     */
//    public void slideshow(byte actionType)
//    {
//        if(listener != null)
//        {
//            listener.slideshow(actionType);
//        }
//    }

    /**
     * exit slideshow
     */
    public void exit() {
        if (listener != null) {
            listener.exit();
        }
    }

    public void dispose() {
        listener = null;
    }

    //
    private SlideShowListener listener;
}
