
package com.atom.office.macro;

import android.graphics.Bitmap;

/**
 * office to picture listener

 * <p>
 * 日期:            2012-9-10

 *

 */
public interface OfficeToPictureListener {

    /**
     * Get converter to of picture Bitmap instance, if the return is empty, is not generated picture
     *
     * @param componentWidth  engine component width
     * @param componentHeight engine component height
     * @return Bitmap instance
     */
    public Bitmap getBitmap(int componentWidth, int componentHeight);

    /**
     * picture generated, the callback method
     *
     * @param bitmap generated picture bitmap
     */
    public void callBack(Bitmap bitmap);
}
