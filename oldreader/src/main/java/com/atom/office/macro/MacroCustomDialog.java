package com.atom.office.macro;

import com.atom.office.common.ICustomDialog;

/**
 * TODO: 文件注释

 * <p>
 * 日期:            2012-12-21

 *

 */
public class MacroCustomDialog implements ICustomDialog {
    /**
     *
     *
     */
    protected MacroCustomDialog(DialogListener listener) {
        this.dailogListener = listener;
    }

    /**
     *
     *
     */
    public void showDialog(byte type) {
        if (dailogListener != null) {
            dailogListener.showDialog(type);
        }
    }

    /**
     *
     *
     */
    public void dismissDialog(byte type) {
        if (dailogListener != null) {
            dailogListener.dismissDialog(type);
        }

    }

    public void dispose() {
        dailogListener = null;
    }

    //
    private DialogListener dailogListener;

}
