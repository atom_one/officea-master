
package com.atom.office.common;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * TODO: 文件注释
 * 日期:            2011-12-12

 *

 */
public class CriticalLock {
    private CriticalLock() {
    }

    public static void lock() {
        reentrantLock.lock();
    }

    public static void unlock() {
        reentrantLock.unlock();
    }

    private static Lock reentrantLock = new ReentrantLock();
}
