
package com.atom.office.common.shape;

import com.atom.office.common.autoshape.ExtendPath;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: 文件注释

 * 日期:            2012-10-9
 * <p>
 *


 */
public class ArbitraryPolygonShape extends LineShape
{   
    public ArbitraryPolygonShape()
    {
        paths = new ArrayList<ExtendPath>();
    }
    
    public void appendPath(ExtendPath path)
    {
        this.paths.add(path);
    }
    
    public List<ExtendPath> getPaths()
    {
        return paths;
    }
    
    //
    private List<ExtendPath> paths;
}
