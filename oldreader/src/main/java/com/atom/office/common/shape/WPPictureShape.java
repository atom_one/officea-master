
package com.atom.office.common.shape;

/**
 * TODO: 文件注释

 * 日期:            2013-5-29
 * <p>
 *


 */
public class WPPictureShape extends WPAutoShape
{

    /**
     * 
     *
     */
    public short getType()
    {
        return SHAPE_PICTURE;
    }
    
    public void setPictureShape(PictureShape pictureShape)
    {
        this.pictureShape = pictureShape;
        
        if(rect == null)
        {
        	rect = pictureShape.getBounds();
        }
    }
    
    public PictureShape getPictureShape()
    {
        return pictureShape;
    }
    

    /**
     * 
     */
    public boolean isWatermarkShape()
    {
        return false;
    }
    
    public void dispose()
    {
        if(pictureShape != null)
        {
            pictureShape.dispose();
            pictureShape = null;
        }
    }
    
    private PictureShape pictureShape;
}
