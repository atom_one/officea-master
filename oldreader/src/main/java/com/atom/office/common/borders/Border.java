

package com.atom.office.common.borders;

/**
 * border

 * Read版本:          Office engine V1.0
 * <p>
 * 作者:              ljj8494
 * <p>
 * 日期:              2013-3-18
 * <p>
 * 负责人:             ljj8494
   TMC

 */

public class Border
{
    /**
     * 
     * @return
     */
    public int getColor()
    {
        return color;
    }

    /**
     * 
     */
    public void setColor(int color)
    {
        this.color = color;
    }

    /**
     * 
     * @return
     */
    public int getLineWidth()
    {
        return lineWidth;
    }

    /**
     */
    public void setLineWidth(int lineWidth)
    {
        this.lineWidth = lineWidth;
    }

    /**
     * 
     * @return
     */
    public byte getLineType()
    {
        return lineType;
    }

    /**
     * 
     */
    public void setLineType(byte lineType)
    {
        this.lineType = lineType;
    }

    /**
     * 
     * @return
     */
    public short getSpace()
    {
        return space;
    }

    /**
     */
    public void setSpace(short space)
    {
        this.space = space;
    }

    //
    private int color;
    //
    private int lineWidth;
    //
    private byte lineType;
    //
    private short space;

}
