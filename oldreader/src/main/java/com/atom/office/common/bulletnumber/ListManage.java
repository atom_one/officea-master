
package com.atom.office.common.bulletnumber;

import java.util.LinkedHashMap;
import java.util.Set;

/**
 * bullet and number manage

 * <p>
 * 日期:            2012-6-18
 * <p>
 * 负责人:          ljj8494


 */
public class ListManage
{
    
    public ListManage()
    {
        lists = new LinkedHashMap<Integer, ListData>();
    }
    
    /**
     * 
     */
    public int putListData(Integer key, ListData value)
    {
        lists.put(key, value);
        return lists.size() - 1;
    }
    
    /**
     * 
     */
    public ListData getListData(Integer key)
    {
        return lists.get(key);
    }
    
    /**
     * 
     */
    public void resetForNormalView()
    {
        Set<Integer> sets = lists.keySet();
        for (Integer key : sets)
        {
            ListData list = lists.get(key);
            if (list != null)
            {
                list.setNormalPreParaLevel((byte)0);
                list.resetForNormalView();
            }
        }
    }
    
    /**
     * 
     */
    public void dispose()
    {
        if (lists != null)
        {
            Set<Integer> sets = lists.keySet();
            for (Integer key : sets)
            {
                lists.get(key).dispose();
            }
            lists.clear();
        }
    }
    
    //
    private LinkedHashMap<Integer, ListData> lists;
}
