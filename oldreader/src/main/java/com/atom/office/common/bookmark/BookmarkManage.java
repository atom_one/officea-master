
package com.atom.office.common.bookmark;

import java.util.HashMap;
import java.util.Map;

/**
 * book mark manage

 * Read版本:        	Office engine V1.0
 * <p>
 * 作者:            	ljj8494
 * <p>
 * 日期:            	2013-5-9
 * <p>
 * 负责人:          	ljj8494
 * <p>
 *

 */
public class BookmarkManage {
    /**
     *
     */
    public BookmarkManage() {
        bms = new HashMap<String, Bookmark>();
    }

    /**
     *
     */
    public void addBookmark(Bookmark bm) {
        bms.put(bm.getName(), bm);
    }

    /**
     *
     */
    public Bookmark getBookmark(String name) {
        return bms.get(name);
    }

    /**
     *
     */
    public int getBookmarkCount() {
        return bms.size();
    }

    /**
     *
     */
    public void dispose() {
        if (bms != null) {
            bms.clear();
            bms = null;
        }
    }

    //
    private Map<String, Bookmark> bms;
}
