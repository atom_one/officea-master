
package com.atom.office.common.shape;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: 文件注释

 * 日期:            2013-4-17
 * <p>
 *


 */
public class SmartArt extends AbstractShape
{

    /**
     * 
     */
    public SmartArt()
    {
        shapes = new ArrayList<IShape>();
    }
    
    /**
     * 
     *
     */
    public short getType()
    {
        return SHAPE_SMARTART;
    }
    
    /**
     * append shape of this slide
     */
    public void appendShapes(IShape shape)
    {
        this.shapes.add(shape);
    }
    
    /**
     * get all shapes of this slide
     */
    public IShape[] getShapes()
    {
        return shapes.toArray(new IShape[shapes.size()]);
    }
    
    private int offX, offY;
    // shapes of this slide
    private List<IShape> shapes;    
}
