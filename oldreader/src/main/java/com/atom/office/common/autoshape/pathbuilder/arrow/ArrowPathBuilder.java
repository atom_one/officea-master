
package com.atom.office.common.autoshape.pathbuilder.arrow;

import android.graphics.Rect;

import com.atom.office.common.shape.AutoShape;

/**
 * TODO: LeftArrow, RightArrow, UpArrow, DownArrow, LeftRightArrow, UpDownArrow

 * 日期:            2012-9-17

 *

 */
public class ArrowPathBuilder {
    /**
     * get autoshape path
     *
     * @param shape
     * @param rect
     * @return
     */
    public static Object getArrowPath(AutoShape shape, Rect rect) {
        if (shape.isAutoShape07()) {
            return LaterArrowPathBuilder.getArrowPath(shape, rect);
        } else {
            return EarlyArrowPathBuilder.getArrowPath(shape, rect);
        }
    }
}
