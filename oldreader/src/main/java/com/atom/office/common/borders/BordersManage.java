
package com.atom.office.common.borders;

import java.util.ArrayList;
import java.util.List;


/**
 * border manage

 * Read版本:        	Office engine V1.0
 * <p>
 * 作者:            	ljj8494
 * <p>
 * 日期:            	2013-3-18
 * <p>
 * 负责人:          	ljj8494
 * <p>
 *        	TMC

 */
public class BordersManage
{
    /**
     * 
     */
    public int addBorders(Borders bs)
    {
        int size = borders.size();
        borders.add(bs);
        return size;
    }
    
    /**
     * 
     * @param index
     * @return
     */
    public Borders getBorders(int index)
    {
        return borders.get(index);
    }
    
    /**
     * 
     */
    public void dispose()
    {
        if (borders != null)
        {
            borders.clear();
            borders = null;
        }
    }
    
    private List<Borders> borders = new ArrayList<Borders>();
}
