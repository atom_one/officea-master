
package com.atom.office.system;

/**
 * 查找接口
 */
public interface IFind {
    /**
     * @param value
     * @return true: finded  false: not finded
     */
    public boolean find(String value);

    /**
     * need call function find first and finded
     *
     * @return
     */
    public boolean findBackward();

    /**
     * need call function find first and finded
     *
     * @return
     */
    public boolean findForward();


    /**
     *
     */
    public int getPageIndex();

    /**
     *
     */
    public void resetSearchResult();

    /**
     *
     */
    public void dispose();
}
