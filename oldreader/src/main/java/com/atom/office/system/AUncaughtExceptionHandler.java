
package com.atom.office.system;

import java.lang.Thread.UncaughtExceptionHandler;


/**
 * 异常处理
 */
public class AUncaughtExceptionHandler implements UncaughtExceptionHandler {

    /**
     *
     */
    public AUncaughtExceptionHandler(IControl control) {
        this.control = control;
    }

    /**
     *
     *
     */
    public void uncaughtException(Thread thread, final Throwable ex) {
        control.getSysKit().getErrorKit().writerLog(ex);
    }

    /**
     *
     */
    public void dispose() {
        control = null;
    }

    /**
     *
     */
    private IControl control;
}
