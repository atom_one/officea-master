
package com.atom.office.system;

/**
 * 中断文件解析异常
 */
public class AbortReaderError extends Error {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public AbortReaderError(String message) {
        super(message);
    }
}
