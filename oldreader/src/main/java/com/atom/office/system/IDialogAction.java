
package com.atom.office.system;

import java.util.Vector;


/**
 * 对话框的Action接口

 */
public interface IDialogAction {
    /**
     * @param id  对话框的ID
     * @param obj 回调action需要数据
     */
    public void doAction(int id, Vector<Object> model);

    /**
     * @return
     */
    public IControl getControl();

    /**
     *
     */
    public void dispose();
}
