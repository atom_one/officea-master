
package com.atom.office.system;


/**
 * 定时器监听器

 */
public interface ITimerListener {
    /**
     * 定时器
     */
    public void actionPerformed();
}
