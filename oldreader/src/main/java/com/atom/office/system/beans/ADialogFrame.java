
package com.atom.office.system.beans;

import android.content.Context;
import android.content.res.Configuration;
import android.widget.LinearLayout;

/**
 * 文件注释

 * <p>
 * 日期:            2011-12-8

 *

 */
public class ADialogFrame extends LinearLayout {

    /**
     *
     */
    public ADialogFrame(Context context, ADialog dialog) {
        super(context);
        setOrientation(LinearLayout.VERTICAL);
        this.dialog = dialog;
    }


    /**
     *
     *
     */
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        dialog.onConfigurationChanged(newConfig);
    }

    /**
     *
     */
    public void dispose() {
        dialog = null;
    }

    private ADialog dialog;
}
