
package com.atom.office.system;

import java.io.File;


/**
 * TODO: 文件注释

 */
public class AbstractReader implements IReader {

    /**
     *
     *
     */
    public Object getModel() throws Exception {
        return null;
    }

    /**
     *
     */
    public boolean searchContent(File file, String key) throws Exception {
        return false;
    }

    /**
     *
     *
     */
    public boolean isReaderFinish() {
        return true;
    }

    /**
     *
     *
     */
    public void backReader() throws Exception {
    }

    /**
     * 中断文档解析
     */
    public void abortReader() {
        abortReader = true;
    }

    /**
     * @return
     */
    public boolean isAborted() {
        return abortReader;
    }

    /**
     *
     * @param password password of document
     * @return true: succeed authenticate False: fail authenticate
     */
    /*public boolean authenticate(String password)
    {
        return true;
    }
    
    *//**
     * cancel authenticate
     *//*
     public void cancelAuthenticate()
     {
         
     }*/

    /**
     *
     */
    public IControl getControl() {
        return this.control;
    }

    /**
     *
     *
     */
    public void dispose() {
        control = null;
    }

    //
    protected boolean abortReader;
    //
    protected IControl control;

}
