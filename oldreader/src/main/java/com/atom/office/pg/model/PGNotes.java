
package com.atom.office.pg.model;

/**
 * note data of this slide
 * <p>

 * <p>
 * 日期:            2012-2-13

 *

 */
public class PGNotes {
    /**
     *
     */
    public PGNotes(String notes) {
        this.notes = notes;
    }

    /**
     *
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     *
     */
    public String getNotes() {
        return notes;
    }

    /**
     *
     */
    public void dispose() {
        notes = null;
    }

    //
    private String notes;
}
