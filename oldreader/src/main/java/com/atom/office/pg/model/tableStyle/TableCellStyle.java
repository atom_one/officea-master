
package com.atom.office.pg.model.tableStyle;

import com.atom.office.fc.dom4j.Element;
import com.atom.office.simpletext.model.IAttributeSet;

/**
 * TODO: 文件注释

 * 日期:            2013-3-22

 *

 */
public class TableCellStyle {
    public TableCellBorders getTableCellBorders() {
        return cellBorders;
    }

    public void setTableCellBorders(TableCellBorders cellBorders) {
        this.cellBorders = cellBorders;
    }

    public Element getTableCellBgFill() {
        return bgFill;
    }

    public void setTableCellBgFill(Element bgFill) {
        this.bgFill = bgFill;
    }

    public void setFontAttributeSet(IAttributeSet fontAttr) {
        this.fontAttr = fontAttr;
    }

    public IAttributeSet getFontAttributeSet() {
        return fontAttr;
    }

    public void dispose() {
        cellBorders = null;
        bgFill = null;
        fontAttr = null;
    }

    private TableCellBorders cellBorders;
    private Element bgFill;
    private IAttributeSet fontAttr;
}
