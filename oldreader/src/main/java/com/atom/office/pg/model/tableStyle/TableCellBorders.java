
package com.atom.office.pg.model.tableStyle;

import com.atom.office.fc.dom4j.Element;

/**
 * TODO: 文件注释

 * 日期:            2013-3-22

 *

 */
public class TableCellBorders {

    public Element getLeftBorder() {
        return left;
    }

    public void setLeftBorder(Element left) {
        this.left = left;
    }

    public Element getTopBorder() {
        return top;
    }

    public void setTopBorder(Element top) {
        this.top = top;
    }

    public Element getRightBorder() {
        return right;
    }

    public void setRightBorder(Element right) {
        this.right = right;
    }

    public Element getBottomBorder() {
        return bottom;
    }

    public void setBottomBorder(Element bottom) {
        this.bottom = bottom;
    }

    private Element left;
    private Element top;
    private Element right;
    private Element bottom;
}
