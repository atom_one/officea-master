
package com.atom.office.wp.view;

import com.atom.office.simpletext.control.IWord;
import com.atom.office.simpletext.view.IRoot;
import com.atom.office.simpletext.view.IView;

/**
 * 后台布局线程

 * <p>
 * 日期:            2011-11-17

 *

 */
public class LayoutThread extends Thread {
    private boolean isDied;

    /**
     * @param root
     */
    public LayoutThread(IRoot root) {
        this.root = root;
    }

    /**
     *
     */
    public void run() {
        while (true) {
            if (isDied) {
                break;
            }
            try {
                if (root.canBackLayout()) {
                    root.backLayout();
                    sleep(50);
                    continue;
                } else {
                    sleep(1000);
                }
            } catch (Exception e) {
                if (root != null) {
                    IWord word = ((IView) root).getContainer();
                    if (word != null && word.getControl() != null) {
                        word.getControl().getSysKit().getErrorKit().writerLog(e);
                    }
                }
                break;
            }
        }
    }

    /**
     *
     */
    public void setDied() {
        isDied = true;
    }

    /**
     *
     */
    public void dispose() {
        root = null;
        isDied = true;
    }

    private IRoot root;
}
