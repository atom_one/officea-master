
package com.atom.office.wp.model;

import com.atom.office.simpletext.model.AbstractElement;

/**
 * 页眉、页脚元素

 * <p>
 * 日期:            2011-12-30

 *

 */
public class HFElement extends AbstractElement {

    /**
     * @param elemType element type
     * @param hftype   first, odd, even
     */
    public HFElement(short elemType, byte hftype) {
        super();
        this.hfType = hftype;
        this.elemType = elemType;
    }

    /**
     * (non-Javadoc)
     *
     * @see com.atom.office.simpletext.model.AbstractElement#getType()
     */
    public short getType() {
        return elemType;
    }

    /**
     * @return Returns the type.
     */
    public byte getHFType() {
        return hfType;
    }

    // first, odd, even
    private byte hfType;
    //
    private short elemType;
}
