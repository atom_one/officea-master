
package com.atom.office.wp.view;

import com.atom.office.constant.wp.WPViewConstant;
import com.atom.office.simpletext.control.IWord;
import com.atom.office.simpletext.model.IDocument;
import com.atom.office.simpletext.model.IElement;
import com.atom.office.simpletext.view.AbstractView;
import com.atom.office.simpletext.view.IView;
import com.atom.office.system.IControl;

/**
 * header、footer view

 * <p>
 * 日期:            2012-7-4

 *

 */
public class TitleView extends AbstractView {

    /**
     * @param elem
     */
    public TitleView(IElement elem) {
        super();
        this.elem = elem;
    }

    /**
     *
     */
    public short getType() {
        return WPViewConstant.TITLE_VIEW;
    }

    /**
     * 得到组件
     */
    public IWord getContainer() {
        if (pageRoot != null) {
            return pageRoot.getContainer();
        }
        return null;
    }

    /**
     * 得到组件
     */
    public IControl getControl() {
        if (pageRoot != null) {
            return pageRoot.getControl();
        }
        return null;
    }

    /**
     * 得到model
     */
    public IDocument getDocument() {
        if (pageRoot != null) {
            return pageRoot.getDocument();
        }
        return null;
    }

    /**
     *
     */
    public void setPageRoot(IView root) {
        pageRoot = root;
    }

    public void dispose() {
        super.dispose();
        pageRoot = null;
    }

    /**
     *
     */
    private IView pageRoot;
}
