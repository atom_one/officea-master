
package com.atom.office.wp.model;

import com.atom.office.constant.wp.WPModelConstant;

/**
 * model kit

 * <p>
 * 日期:            2012-5-9

 *

 */
public class ModelKit {
    private static ModelKit kit = new ModelKit();

    /**
     *
     */
    public static ModelKit instance() {
        return kit;
    }

    /**
     * @return
     */
    public long getArea(long offset) {
        return offset & WPModelConstant.AREA_MASK;
    }

}
