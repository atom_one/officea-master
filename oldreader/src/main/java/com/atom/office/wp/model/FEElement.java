
package com.atom.office.wp.model;

import com.atom.office.simpletext.model.AbstractElement;

/**
 * 脚注、尾注Element

 * <p>
 * 日期:            2011-12-30

 *

 */
public class FEElement extends AbstractElement {

    /**
     *
     */
    public FEElement() {
        super();
    }
}
