
package com.atom.office.wp.model;

import com.atom.office.constant.wp.WPModelConstant;
import com.atom.office.simpletext.model.AbstractElement;

/**
 * table cell element

 * <p>
 * 日期:            2012-4-17

 *

 */
public class CellElement extends AbstractElement {
    /**
     *
     */
    public CellElement() {
        super();
    }

    /**
     *
     */
    public short getType() {
        return WPModelConstant.TABLE_CELL_ELEMENT;
    }
}
