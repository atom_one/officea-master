
package com.atom.office.wp.control;

import com.atom.office.constant.DialogConstant;
import com.atom.office.system.IControl;
import com.atom.office.system.IDialogAction;

import java.util.Vector;


/**
 * WP用到dialogAction

 */
public class WPDialogAction implements IDialogAction {

    public WPDialogAction(IControl control) {
        this.control = control;
    }

    /**
     * @param id  对话框的ID
     * @param model 回调action需要数据
     */
    public void doAction(int id, Vector<Object> model) {
        switch (id) {
            case DialogConstant.ENCODING_DIALOG_ID:
                break;

            default:
                break;
        }
    }

    /**
     *
     */
    public IControl getControl() {
        return this.control;
    }

    /**
     *
     */
    public void dispose() {
        control = null;
    }

    //
    public IControl control;
}
