
package com.atom.office.wp.control;

import com.atom.office.common.shape.AbstractShape;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * WP管理器

 * <p>
 * 日期:            2012-4-5

 *

 */
public class WPShapeManage {
    /**
     *
     */
    public WPShapeManage() {
        shapes = new HashMap<Integer, AbstractShape>(20);
    }

    /**
     *
     */
    public int addShape(AbstractShape shape) {
        int size = shapes.size();
        shapes.put(size, shape);
        return size;
    }

    /**
     *
     */
    public AbstractShape getShape(int index) {
        if (index < 0 || index >= shapes.size()) {
            return null;
        }
        return shapes.get(index);
    }

    /**
     *
     */
    public void dispose() {
        if (shapes != null) {
            Collection<AbstractShape> ass = shapes.values();
            if (ass != null) {
                for (AbstractShape as : ass) {
                    as.dispose();
                }
                shapes.clear();
            }
        }
    }


    //
    private Map<Integer, AbstractShape> shapes;

}
