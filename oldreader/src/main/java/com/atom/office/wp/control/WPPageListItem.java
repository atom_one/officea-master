
package com.atom.office.wp.control;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.atom.office.system.IControl;
import com.atom.office.system.beans.pagelist.APageListItem;
import com.atom.office.system.beans.pagelist.APageListView;
import com.atom.office.wp.view.PageRoot;
import com.atom.office.wp.view.PageView;

/**
 * word engine "PageListView" component item

 */
public class WPPageListItem extends APageListItem {
    private static final int BACKGROUND_COLOR = 0xFFFFFFFF;


    /**
     *
     * @param listView
     * @param control
     * @param pageWidth
     * @param pageHeight
     */
    public WPPageListItem(APageListView listView, IControl control, int pageWidth, int pageHeight) {
        super(listView, pageWidth, pageHeight);
        this.control = control;
        this.pageRoot = (PageRoot) listView.getModel();
        this.setBackgroundColor(BACKGROUND_COLOR);
    }

    /**
     *
     */
    public void onDraw(Canvas canvas) {
        PageView pv = pageRoot.getPageView(pageIndex);
        if (pv != null) {
            float zoom = listView.getZoom();
            canvas.save();
            canvas.translate(-pv.getX() * zoom, -pv.getY() * zoom);
            pv.drawForPrintMode(canvas, 0, 0, zoom);
            canvas.restore();
        }
    }

    /**
     * @param pIndex  page index (base 0)
     * @param pageWidth  page width of after scaled
     * @param pageHeight page height of after scaled
     */
    public void setPageItemRawData(final int pIndex, int pageWidth, int pageHeight) {
        super.setPageItemRawData(pIndex, pageWidth, pageHeight);
        //final APageListItem own = this;
        if ((int) (listView.getZoom() * 100) == 100
                || (isInit && pIndex == 0)) {
            listView.exportImage(this, null);
        }
        isInit = false;
    }

    /**
     * added reapint image view
     */
    protected void addRepaintImageView(Bitmap bmp) {
        postInvalidate();
        listView.exportImage(this, null);
    }

    /**
     * remove reapint image view
     */
    protected void removeRepaintImageView() {

    }


    /**
     *
     */
    public void dispose() {
        super.dispose();
        control = null;
        pageRoot = null;
    }

    //
    private boolean isInit = true;
    //
    private PageRoot pageRoot;

}
