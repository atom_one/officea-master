
package com.atom.office.wp.model;

import com.atom.office.constant.wp.WPModelConstant;
import com.atom.office.simpletext.model.ElementCollectionImpl;
import com.atom.office.simpletext.model.IDocument;
import com.atom.office.simpletext.model.IElement;
import com.atom.office.simpletext.model.LeafElement;
import com.atom.office.simpletext.model.ParagraphElement;

/**
 * table element

 * <p>
 * 日期:            2012-4-17

 *

 */
public class TableElement extends ParagraphElement {

    /**
     *
     */
    public TableElement() {
        super();
        rowElement = new ElementCollectionImpl(10);
    }

    /**
     *
     */
    public short getType() {
        return WPModelConstant.TABLE_ELEMENT;
    }

    /**
     *
     */
    public void appendRow(RowElement rowElem) {
        rowElement.addElement(rowElem);
    }

    /**
     *
     */
    public IElement getRowElement(long offset) {
        return rowElement.getElement(offset);
    }

    /**
     * 得到指定index的Offset
     */
    public IElement getElementForIndex(int index) {
        return rowElement.getElementForIndex(index);
    }

    /**
     *
     *
     */
    public String getText(IDocument doc) {
        return "";
    }

    /**
     *
     */
    public void appendLeaf(LeafElement leafElem) {
    }

    /**
     *
     */
    public IElement getLeaf(long offset) {
        return null;
    }

    private ElementCollectionImpl rowElement;

}
