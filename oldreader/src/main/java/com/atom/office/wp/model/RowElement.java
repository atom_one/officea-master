
package com.atom.office.wp.model;

import com.atom.office.constant.wp.WPModelConstant;
import com.atom.office.simpletext.model.AbstractElement;
import com.atom.office.simpletext.model.ElementCollectionImpl;
import com.atom.office.simpletext.model.IElement;

/**
 * table row element

 * <p>
 * 日期:            2012-4-17

 *

 */
public class RowElement extends AbstractElement {
    /**
     *
     */
    public RowElement() {
        super();
        cellElement = new ElementCollectionImpl(10);
    }

    /**
     *
     */
    public short getType() {
        return WPModelConstant.TABLE_ROW_ELEMENT;
    }

    /**
     *
     */
    public void appendCell(CellElement cellElem) {
        cellElement.addElement(cellElem);
    }

    /**
     *
     */
    public IElement getCellElement(long offset) {
        return cellElement.getElement(offset);
    }

    /**
     * 得到指定index的Offset
     */
    public IElement getElementForIndex(int index) {
        return cellElement.getElementForIndex(index);
    }

    /**
     * 插入Element至指定的index
     *
     * @param element
     * @param index
     */
    public void insertElementForIndex(IElement element, int index) {
        cellElement.insertElementForIndex(element, index);
    }

    /**
     *
     */
    public int getCellNumber() {
        return cellElement.size();
    }

    //
    private ElementCollectionImpl cellElement;

}
