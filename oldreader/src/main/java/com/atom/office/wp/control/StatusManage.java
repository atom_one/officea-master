
package com.atom.office.wp.control;

/**
 * word application status

 * <p>
 * 日期:            2012-2-28

 *

 */
public class StatusManage {

    /**
     * @return Returns the selectText.
     */
    public boolean isSelectTextStatus() {
        return selectText;
    }

    /**
     * @param selectText The selectText to set.
     */
    public void setSelectTextStatus(boolean selectText) {
        this.selectText = selectText;
    }

    /**
     * @return Returns the pressOffset.
     */
    public long getPressOffset() {
        return pressOffset;
    }

    /**
     * @param pressOffset The pressOffset to set.
     */
    public void setPressOffset(long pressOffset) {
        this.pressOffset = pressOffset;
    }

    /**
     * @return Returns the isTouchDown.
     */
    public boolean isTouchDown() {
        return isTouchDown;
    }

    /**
     * @param isTouchDown The isTouchDown to set.
     */
    public void setTouchDown(boolean isTouchDown) {
        this.isTouchDown = isTouchDown;
    }

    /**
     *
     */
    public void dispose() {

    }

    // select text status
    private boolean selectText;
    // touch down offset 
    private long pressOffset;
    // touch down status
    private boolean isTouchDown;

}
