
package com.atom.office.wp.view;

import com.atom.office.wp.model.CellElement;

/**
 * 布局过程记录跨页的单元格

 * <p>
 * 日期:            2012-6-11

 *

 */
public class BreakPagesCell {

    public BreakPagesCell(CellElement cell, long breakOffset) {
        this.cell = cell;
        this.breakOffset = breakOffset;
    }


    /**
     *
     */
    public CellElement getCell() {
        return cell;
    }

    /**
     *
     */
    public long getBreakOffset() {
        return breakOffset;
    }

    // across patges cell;
    private CellElement cell;
    // across pages offset
    private long breakOffset;

}
