
package com.atom.office.objectpool;

/**
 * 共享对象接口

 * <p>
 * 日期:            2012-8-21

 *

 */
public interface IMemObj {

    /**
     * 放回对象池
     */
    public void free();

    /**
     * 复制对象
     */
    public IMemObj getCopy();
}
