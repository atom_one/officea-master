
package com.atom.office.objectpool;

import java.util.Vector;

/**
 * token manage

 * <p>
 * 日期:            2012-9-3

 *

 */
public class TokenManage {
    // token size
    private static final int TOKEN_SIZE = 10;
    //
    public static TokenManage kit = new TokenManage();

    /**
     *
     */
    public static TokenManage instance() {
        return kit;
    }

    /**
     *
     */
    public synchronized ParaToken allocToken(IMemObj obj) {
        ParaToken token = null;
        if (paraTokens.size() >= TOKEN_SIZE) {
            for (int i = 0; i < TOKEN_SIZE; i++) {
                if (paraTokens.get(i).isFree()) {
                    token = paraTokens.remove(i);
                    break;
                }
            }
            //token.free();
            paraTokens.add(token);
        } else {
            token = new ParaToken(obj);
            paraTokens.add(token);

        }
        return token;
    }

    //
    public Vector<ParaToken> paraTokens = new Vector<ParaToken>(TOKEN_SIZE);
}
