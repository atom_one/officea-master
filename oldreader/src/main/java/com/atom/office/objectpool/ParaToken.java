
package com.atom.office.objectpool;

/**
 * 段落视图的令牌

 * <p>
 * 日期:            2012-8-22

 *

 */
public class ParaToken {

    /**
     * @param obj
     */
    public ParaToken(IMemObj obj) {
        this.obj = obj;
    }

    /**
     *
     *
     */
    public void free() {
        obj.free();
    }

    /**
     * @return Returns the using.
     */
    public boolean isFree() {
        return isFree;
    }

    /**
     * @param free The using to set.
     */
    public void setFree(boolean free) {
        this.isFree = free;
    }

    /**
     *
     */
    public void dispose() {
        obj = null;
    }


    private boolean isFree;
    //
    private IMemObj obj;
}
