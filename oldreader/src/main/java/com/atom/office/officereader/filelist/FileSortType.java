
package com.atom.office.officereader.filelist;

/**
 * file sort type

 * Read版本:       Read V1.0
 * <p>
 * 作者:           jhy1790
 * <p>
 * 日期:           2012-1-9
 * <p>
 * 负责人:         jhy1790


 */
public class FileSortType
{
    /**
     * 
     */
    public FileSortType()
    {
      recentType = -1;  
    }
  
    /**
     * 
     * @return
     */
    public int getSdcardType()
    {
        return sdcardType;      
    }
    
    /**
     * 
     * @return
     */
    public int getRecentType()
    {
        return recentType;       
    }
    
    /**
     * 
     * @return
     */
    public int getStarType()
    {
        return starType;        
    }
    
   /**
    * 
    * @return
    */
    public int getSdcardAscending()
    {
        return sdcardAscending;        
    }
    
    /**
     * 
     * @return
     */
    public int getRecentAscending()
    {
        return recentAscending;        
    }
    
    /**
     * 
     * @return
     */
    public int getStarAscending()
    {
        return starAscending;        
    }
    
    /**
     * 
     * @param type
     * @param child
     */
    public void setSdcardType(int type, int ascending)
    {
        sdcardType = type;
        sdcardAscending = ascending;        
    }
    
    /**
     * 
     * @param type
     * @param child
     */
    public void setRecentType(int type, int ascending)
    {
        recentType = type;
        recentAscending = ascending;        
    }
    
    /**
     * 
     * @param type
     * @param child
     */
    public void setStarType(int type, int ascending)
    {
        starType = type;
        starAscending = ascending;        
    }
   
    /**
     * 
     */
    public void dispos()
    {
        
    }
    
    //
    private int sdcardType;
    //
    private int recentType;
    //
    private int starType;
    //
    private int sdcardAscending;
    //
    private int recentAscending;
    //
    private int starAscending;
}
