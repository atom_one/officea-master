
package com.atom.office.officereader;

import android.app.Activity;

import com.atom.office.system.AbstractControl;
import com.atom.office.system.SysKit;

/**
 * file list control

 */
public class FileListControl extends AbstractControl {

    /**
     * @param activity
     */
    public FileListControl(Activity activity) {
        this.activity = activity;
    }

    /**
     *
     *
     */
    public void actionEvent(int actionID, Object obj) {
        ((FileListActivity) activity).actionEvent(actionID, obj);
    }

    /**
     *
     */
    public Activity getActivity() {
        return activity;
    }

    /**
     *
     */
    public SysKit getSysKit() {
        if (sysKit == null) {
            sysKit = new SysKit(this);
        }
        return this.sysKit;
    }

    /**
     *
     */
    public void dispose() {
        activity = null;
        sysKit = null;
    }

    //
    private Activity activity;
    private SysKit sysKit;
}
