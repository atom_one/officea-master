
package com.atom.office.officereader;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

import com.atom.office.system.IControl;

public class SysActivity extends AppCompatActivity {

    //
    private SysFrame sysFrame;
    //
    private IControl control;

    /**
     * 构造器
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        control = new SysControl(this);
        sysFrame = new SysFrame(this, control);
        sysFrame.post(new Runnable() {
            /**
             */
            public void run() {
                init();
            }
        });

        setContentView(sysFrame);
    }

    /**
     *
     */
    public void init() {
        sysFrame.init();
    }

    /**
     *
     *
     */
    public View getSysFrame() {
        return sysFrame;
    }

    /**
     *
     */
    public void onBackPressed() {
        super.onBackPressed();
        System.exit(0);
    }

    /**
     *
     *
     */
    public void dispose() {
        sysFrame.dispose();
        control.dispose();
    }


}
