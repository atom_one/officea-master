package com.atom.office.officereader;

import android.content.Context;
import android.widget.LinearLayout;

/**
 * 文件列表视图容器

 * <p>
 * 日期:            2011-12-9

 *

 */
public class FileFrame extends LinearLayout {

    /**
     * @param context
     */
    public FileFrame(Context context) {
        super(context);
        setOrientation(LinearLayout.VERTICAL);
    }

    /**
     *
     */
    public void dispose() {

    }
}
