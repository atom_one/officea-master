
package com.atom.office.officereader.search;

import java.io.File;

/**
 * 搜索接口

 * <p>
 * 日期:            2012-3-26
 * <p>
 * 负责人:          ljj8494


 */
public interface ISearchResult
{
    /**
     * return to Search Results. call with has match file
     * 
     * @param 
     */
    public void onResult(File file);
    
    /**
     * call with search finish
     */
    public void searchFinish();
    
}
