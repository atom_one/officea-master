
package com.atom.office.officereader.filelist;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.atom.office.officereader.FileListActivity;
import com.atom.office.system.IControl;

/**
 * 文件选择对话框

 * <p>
 * 日期:            2011-11-29

 *

 */
public class FileCheckBox extends CheckBox {
    /**
     * @param context
     */
    public FileCheckBox(Context context, IControl control, FileItem fileItem) {
        super(context);
        this.fileItem = fileItem;
        this.control = control;
        this.setChecked(fileItem.isCheck());
        this.setFocusable(false);
    }

    /**
     * @param context
     * @param attrs
     */
    public FileCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     *
     */
    public boolean performClick() {
        boolean b = super.performClick();
        fileItem.setCheck(isChecked());
        FileListActivity activity = (FileListActivity) control.getActivity();
        if (isChecked()) {
            activity.addSelectFileItem(fileItem);
        } else {
            activity.removeSelectFileItem(fileItem);
        }
        return b;
    }

    /**
     *
     */
    public void setFileItem(FileItem fileItem) {
        this.fileItem = fileItem;
        setChecked(fileItem.isCheck());
    }

    /**
     *
     */
    public void dispose() {
        fileItem = null;
        control = null;
    }

    //
    private FileItem fileItem;
    //
    private IControl control;

}
