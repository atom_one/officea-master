
package com.atom.office.officereader.beans;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.atom.office.officereader.R;

/**
 * single choice list

 * Read版本:       Read V1.0
 * <p>
 * 作者:           jhy1790
 * <p>
 * 日期:           2011-12-16
 * <p>
 * 负责人:         jhy1790


 */
public class SingleChoiceList extends ListView
{
    /**
     * 
     * @param context
     * @param itemsResID
     */
    public SingleChoiceList(Context context, int itemsResID)
    {
        super(context);
        setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        String[] items = context.getResources().getStringArray(itemsResID);
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(
            context, R.layout.select_dialog_singlechoice, android.R.id.text1, items);
        setAdapter(adapter);
    }
    /**
     * 
     */
    public void dispose()
    {
    }
}
