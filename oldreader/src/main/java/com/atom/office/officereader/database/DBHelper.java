
package com.atom.office.officereader.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.atom.office.constant.MainConstant;

/**
 * 创建数据库，表

 * Read版本:       Read V1.0
 * <p>
 * 作者:           jhy1790
 * <p>
 * 日期:           2011-12-28
 * <p>
 * 负责人:         jhy1790


 */
public class DBHelper extends SQLiteOpenHelper
{
    private static final int DATABASE_VERSION = 1;
    private static final String DB_NAME = "atomReader.db";

    /**
     * 
     * @param context
     */
    public DBHelper(Context context)
    {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    /**
     * 
     *(non-Javadoc)
     * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
     *
     */
    public void onCreate(SQLiteDatabase db)
    {
        if (db != null)
        {
            db.execSQL("CREATE TABLE IF NOT EXISTS " + MainConstant.TABLE_RECENT + " ('name' VARCHAR(30))");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + MainConstant.TABLE_STAR + " ('name' VARCHAR(30))");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + MainConstant.TABLE_SETTING + " ('count' VARCHAR(30))");
        }
    }
 
    /**
     * 
     *(non-Javadoc)
     * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
     *
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        if (db != null)
        {
            db.execSQL("DROP TABLE IF EXISTS " + MainConstant.TABLE_RECENT);
            db.execSQL("DROP TABLE IF EXISTS " + MainConstant.TABLE_STAR);
            db.execSQL("DROP TABLE IF EXISTS " + MainConstant.TABLE_SETTING);
            onCreate(db);
        }
    }
  
    /**
     * 
     */
    public void dispose()
    {
        
    }
}
