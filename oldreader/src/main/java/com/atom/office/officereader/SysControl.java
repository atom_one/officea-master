package com.atom.office.officereader;

import android.app.Activity;
import android.view.View;
import android.widget.Toast;

import com.atom.office.constant.EventConstant;
import com.atom.office.system.AbstractControl;
import com.atom.office.system.SysKit;

/**
 * sys control

 */
public class SysControl extends AbstractControl {

    /**
     * @param activity
     */
    public SysControl(Activity activity) {
        this.activity = activity;
        toast = Toast.makeText(activity, "", Toast.LENGTH_SHORT);
    }

    /**
     *
     *
     */
    public void actionEvent(int actionID, Object obj) {
        switch (actionID) {
            case EventConstant.SYS_SHOW_TOOLTIP:
                if (obj != null && obj instanceof String) {
                    toast.setText((String) obj);
                    toast.show();
                }
                break;
            case EventConstant.SYS_CLOSE_TOOLTIP:
                toast.cancel();
                break;

            case EventConstant.SYS_SEARCH_ID:
                activity.onSearchRequested();
                break;

            default:
                break;
        }
    }

    /**
     *
     *
     */
    public View getView() {
        return ((SysActivity) activity).getSysFrame();
    }

    /**
     *
     *
     */
    public Activity getActivity() {
        return activity;
    }

    /**
     *
     */
    public SysKit getSysKit() {
        if (sysKit == null) {
            sysKit = new SysKit(this);
        }
        return this.sysKit;
    }

    /**
     *
     */
    public void dispose() {
        activity = null;
        sysKit = null;
    }

    //toast
    private Toast toast;
    //
    private Activity activity;
    private SysKit sysKit;
}

