
package com.atom.office.officereader;

import android.content.Context;
import android.widget.LinearLayout;

/**
 * 设置列表视图容器

 * Read版本:       Read V1.0
 * <p>
 * 作者:           jhy1790
 * <p>
 * 日期:           2012-1-16
 * <p>
 * 负责人:         jhy1790
 * <p>
 *

 */
public class SettingFrame extends LinearLayout {
    /**
     * @param context
     */
    public SettingFrame(Context context) {
        super(context);
        setOrientation(LinearLayout.VERTICAL);
    }

    /**
     *
     */
    public void dispose() {

    }
}
