
package com.atom.office.officereader;

import android.content.SearchRecentSuggestionsProvider;

/**
 * search provider
 */
public class SearchSuggestionsProvider extends SearchRecentSuggestionsProvider {
    final static String AUTHORITY = "searchprovider";
    final static int MODE = DATABASE_MODE_QUERIES;

    /**
     *
     */
    public SearchSuggestionsProvider() {
        super();
        setupSuggestions(AUTHORITY, MODE);
    }
}
