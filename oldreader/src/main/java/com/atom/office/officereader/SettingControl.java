
package com.atom.office.officereader;

import android.app.Activity;

import com.atom.office.constant.EventConstant;
import com.atom.office.officereader.database.DBService;
import com.atom.office.system.AbstractControl;
import com.atom.office.system.SysKit;


public class SettingControl extends AbstractControl {
    /**
     * @param activity
     */
    public SettingControl(Activity activity) {
        this.activity = activity;
        dbService = new DBService(activity);
    }

    /**
     *
     *
     */
    public void actionEvent(int actionID, Object obj) {
        switch (actionID) {
            case EventConstant.SYS_SET_MAX_RECENT_NUMBER:
                if (obj != null) {
                    dbService.changeRecentCount((Integer) obj);
                }
                break;
            default:
                break;
        }
    }


    /**
     *
     *
     */
    public Activity getActivity() {
        return activity;
    }

    /**
     * @return
     */
    public int getRecentMax() {
        return dbService == null ? 0 : dbService.getRecentMax();
    }

    /**
     *
     */
    public SysKit getSysKit() {
        if (sysKit == null) {
            sysKit = new SysKit(this);
        }
        return this.sysKit;
    }

    /**
     *
     */
    public void dispose() {
        activity = null;
        if (dbService != null) {
            dbService.dispose();
            dbService = null;
        }
        sysKit = null;
    }

    //
    private Activity activity;
    //
    public DBService dbService;
    private SysKit sysKit;

}
