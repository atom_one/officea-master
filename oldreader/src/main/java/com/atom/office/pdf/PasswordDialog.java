package com.atom.office.pdf;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.atom.office.common.ICustomDialog;
import com.atom.office.constant.EventConstant;
import com.atom.office.fc.pdf.PDFLib;
import com.atom.office.res.ResKit;
import com.atom.office.system.IControl;

/**
 *

 * <p>
 * 日期:            2013-3-4
 * <p>
 * 负责人:          ljj8494


 */
public class PasswordDialog
{
    
    /**
     * 
     */
    public PasswordDialog(IControl control, PDFLib lib)
    {
        this.control = control;
        this.lib = lib;
    }
    
    /**
     * 
     */
    public void show()
    {
        if(control.getMainFrame().isShowPasswordDlg())
        {
            alertBuilder = new AlertDialog.Builder(control.getActivity());
            requestPassword(lib); 
        }
        else
        {
            ICustomDialog dlgListener = control.getCustomDialog();
            if(dlgListener != null)
            {
                dlgListener.showDialog(ICustomDialog.DIALOGTYPE_PASSWORD);
            }
        }
    }

    /**
     * 
     * @param savedInstanceState
     */
    private void requestPassword(final PDFLib lib)
    {
        pwView = new EditText(control.getActivity());
        pwView.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
        pwView.setTransformationMethod(new PasswordTransformationMethod());

        AlertDialog alert = alertBuilder.create();
        alert.setTitle(ResKit.instance().getLocalString("DIALOG_ENTER_PASSWORD"));
        alert.setView(pwView);
        alert.setButton(AlertDialog.BUTTON_POSITIVE, 
            ResKit.instance().getLocalString("BUTTON_OK"), 
            new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                if (lib.authenticatePasswordSync(pwView.getText().toString()))
                {
                    control.actionEvent(EventConstant.APP_PASSWORD_OK_INIT, null);
                    return;
                }
                else
                {
                    requestPassword(lib);
                }
            }
        });
        alert.setButton(AlertDialog.BUTTON_NEGATIVE, 
            ResKit.instance().getLocalString("BUTTON_CANCEL"),
            new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int which)
                {
                    //lib.dispose();
                    control.getActivity().onBackPressed();
                    //control.getMainFrame().destroyEngine();
                }
            });
        
        alert.setOnKeyListener(new DialogInterface.OnKeyListener()
        {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event)
            {
                if (keyCode == KeyEvent.KEYCODE_BACK)
                {
                    dialog.dismiss();
                    control.getActivity().onBackPressed();
                    //control.getMainFrame().destroyEngine();
                    return true;
                }
                return false;
            }
        });
        alert.show();
    }
    
    //
    private IControl control;
    //
    private PDFLib lib;
    //
    private AlertDialog.Builder alertBuilder;
    //
    private EditText pwView;
}
