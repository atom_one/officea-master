
package com.atom.office.constant;

/**
 * PDF engine constant

 * <p>
 * 日期:            2013-1-14
 * <p>
 * 负责人:          ljj8494


 */
public class PDFConstant
{

    public static final int BUSY_SIZE = 60;
    // highlight color
    public static final int HIGHLIGHT_COLOR = 0x805555FF;
    // hyperlink color
    public static final int LINK_COLOR = 0x80FFCC88;
    // background color
    public static final int BACKGROUND_COLOR = 0xFFFFFFFF;
    // progress dialog delay
    public static final int PROGRESS_DIALOG_DELAY = 200;
}
