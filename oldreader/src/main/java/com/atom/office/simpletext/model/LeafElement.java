
package com.atom.office.simpletext.model;

import com.atom.office.constant.wp.WPModelConstant;

/**
 * Leaf Element

 * <p>
 * 日期:            2011-12-28

 *

 */
public class LeafElement extends AbstractElement {
    /**
     *
     */
    public LeafElement(String text) {
        super();
        this.text = text;
    }

    /**
     *
     */
    public short getType() {
        return WPModelConstant.LEAF_ELEMENT;
    }


    /**
     *
     */
    public String getText(IDocument doc) {
        return text;
    }

    /**
     *
     */
    public void setText(String text) {
        this.text = text;
        this.setEndOffset(getStartOffset() + text.length());
    }

    /**
     *
     */
    public void dispose() {
        super.dispose();
        text = null;
    }

    /**
     *
     */

    //
    private String text;
}
