
package com.atom.office.simpletext.model;

import com.atom.office.ss.model.baseModel.Cell;
import com.atom.office.ss.model.baseModel.Workbook;

/**
 * TODO: 文件注释

 * 日期:            2012-4-27

 *

 */
public class CellLeafElement extends LeafElement {

    public CellLeafElement(Cell cell, int offStart, int offEnd) {
        super(null);

        book = cell.getSheet().getWorkbook();

        this.sharedStringIndex = cell.getStringCellValueIndex();
        this.offStart = offStart;
        this.offEnd = offEnd;
    }

    /**
     *
     */
    public String getText(IDocument doc) {
        if (appendNewline) {
            return book.getSharedString(sharedStringIndex).substring(offStart, offEnd) + "\n";
        } else {
            return book.getSharedString(sharedStringIndex).substring(offStart, offEnd);
        }

    }

//    /**
//     * 
//     *
//     */
//    public long getEndOffset()
//    {        
//        if(appendNewline)
//        {
//            return end + 1;
//        }
//        else
//        {
//            return end;
//        }
//        
//    }

    /**
     *
     */
    public void appendNewlineFlag() {
        appendNewline = true;
    }

    /**
     *
     */
    public void dispose() {
        book = null;
    }

    private Workbook book;

    private int sharedStringIndex;

    private int offStart;

    private int offEnd;

    private boolean appendNewline;
}
