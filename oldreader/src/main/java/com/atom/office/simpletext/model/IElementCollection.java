
package com.atom.office.simpletext.model;

/**
 * Element集合接口

 * <p>
 * 日期:            2011-12-29

 *

 */
public interface IElementCollection {
    /**
     * 得到指定Office的Element
     */
    public IElement getElement(long offset);

    /**
     * 得到指定index的Offset
     */
    public IElement getElementForIndex(int index);

    /**
     *
     */
    public int size();

    /**
     *
     */
    public void dispose();

}
