
package com.atom.office.simpletext.view;

/**
 * table attribute

 * <p>
 * 日期:            2012-5-21
 * <p>
 * 负责人:          ljj8494


 */
public class TableAttr
{

    // 上边距
    public int topMargin;
    // 左边距
    public int leftMargin;
    // 右边距
    public int rightMargin;
    // 下边距
    public int bottomMargin;
    // cell宽度
    public int cellWidth;
    // cell vertical align
    public byte cellVerticalAlign;
    // cell background
    public int cellBackground;
    
    /**
     * 
     */
    public void reset()
    {
        topMargin = 0;
        leftMargin = 0;
        rightMargin = 0;
        bottomMargin = 0;
        cellVerticalAlign = 0;
        cellBackground = -1;
    }
    
    
}
