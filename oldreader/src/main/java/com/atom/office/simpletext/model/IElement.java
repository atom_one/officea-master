
package com.atom.office.simpletext.model;

/**
 * Model的元素，主要是章节、段落、Leaf

 * <p>
 * 日期:            2011-11-11

 *

 */
public interface IElement {
    /**
     *
     */
    public short getType();

    /**
     * 开始Offset
     */
    public void setStartOffset(long start);

    public long getStartOffset();

    /**
     * 结束Offset
     */
    public void setEndOffset(long end);

    public long getEndOffset();

    /**
     *
     */
    public void setAttribute(IAttributeSet attrSet);

    /**
     * 得到属性集
     */
    public IAttributeSet getAttribute();

    /**
     * 得到文本
     */
    public String getText(IDocument doc);

    /**
     *
     */
    public void dispose();

}
