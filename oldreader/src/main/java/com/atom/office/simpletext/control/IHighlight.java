
package com.atom.office.simpletext.control;

import android.graphics.Canvas;

import com.atom.office.simpletext.view.IView;

/**
 * highlight

 * <p>
 * 日期:            2012-7-27

 *

 */
public interface IHighlight {

    /**
     * draw highlight
     */
    public void draw(Canvas canvas, IView line, int originX, int originY, long start, long end, float zoom);

    /**
     *
     */
    public String getSelectText();

    /**
     *
     */
    public boolean isSelectText();

    /**
     * remove all selection
     */
    public void removeHighlight();

    /**
     *
     */
    public void addHighlight(long start, long end);

    /**
     * @return Returns the selectStart.
     */
    public long getSelectStart();

    /**
     * @param selectStart The selectStart to set.
     */
    public void setSelectStart(long selectStart);

    /**
     * @return Returns the selectEnd.
     */
    public long getSelectEnd();

    /**
     * @param selectEnd The selectEnd to set.
     */
    public void setSelectEnd(long selectEnd);

    /**
     * @param isPaintHighlight The isPaintHighlight to set.
     */
    public void setPaintHighlight(boolean isPaintHighlight);

    /**
     *
     */
    public void dispose();

}
