
package com.atom.office.simpletext.view;


/**
 * 定义视图接口方法

 * <p>
 * 日期:            2011-11-14

 *

 */
public interface IRoot {
    //the min layout width
    public static final int MINLAYOUTWIDTH = 5;

    /**
     * 是否可以后台
     *
     * @return
     */
    public boolean canBackLayout();

    /**
     * 后台布局
     */
    public void backLayout();

    /**
     *
     */
    public ViewContainer getViewContainer();
}
