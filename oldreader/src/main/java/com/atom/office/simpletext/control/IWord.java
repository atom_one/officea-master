
package com.atom.office.simpletext.control;

import com.atom.office.common.shape.IShape;
import com.atom.office.java.awt.Rectangle;
import com.atom.office.pg.animate.IAnimation;
import com.atom.office.simpletext.model.IDocument;
import com.atom.office.system.IControl;

/**
 * IWord 接口

 * <p>
 * 日期:            2012-7-27

 *

 */
public interface IWord {
    /**
     * @return Returns the highlight.
     */
    public IHighlight getHighlight();

    /**
     *
     */
    public Rectangle modelToView(long offset, Rectangle rect, boolean isBack);

    /**
     *
     */
    public IDocument getDocument();

    /**
     *
     */
    public String getText(long start, long end);

    /**
     * @param x 为100%的值
     * @param y 为100%的值
     */
    public long viewToModel(int x, int y, boolean isBack);

    /**
     *
     */
    public byte getEditType();

    /**
     * @param para
     * @return
     */
    public IAnimation getParagraphAnimation(int pargraphID);

    /**
     *
     */
    public IShape getTextBox();

    /**
     *
     */
    public IControl getControl();

    /**
     *
     */
    public void dispose();
}
