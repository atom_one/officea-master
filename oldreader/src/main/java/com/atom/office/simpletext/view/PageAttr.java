
package com.atom.office.simpletext.view;


/**
 * 页面属性集

 * <p>
 * 日期:            2011-11-20

 *

 */
public class PageAttr {

    public static final byte GRIDTYPE_NONE = 0;
    public static final byte GRIDTYPE_LINE_AND_CHAR = 1;
    public static final byte GRIDTYPE_LINE = 2;
    public static final byte GRIDTYPE_CHAR = 3;

    // 页面宽度
    public int pageWidth;
    // 页面高度
    public int pageHeight;
    // 上边距
    public int topMargin;
    // 下边距
    public int bottomMargin;
    // 左边距
    public int leftMargin;
    // 右边距
    public int rightMargin;
    // page vertical alignment
    public byte verticalAlign;
    //
    public byte horizontalAlign;
    //
    public int headerMargin;
    //
    public int footerMargin;
    //
    public int pageBRColor;
    //
    public int pageBorder;
    //
    public float pageLinePitch;

    /**
     *
     */
    public void reset() {
        verticalAlign = 0;
        horizontalAlign = 0;
        pageWidth = 0;
        pageHeight = 0;
        topMargin = 0;
        bottomMargin = 0;
        leftMargin = 0;
        rightMargin = 0;
        headerMargin = 0;
        footerMargin = 0;
        pageBorder = 0;
        pageBRColor = 0xFFFFFFFF;
        pageLinePitch = 0;
    }

    /**
     *
     */
    public void dispose() {

    }
}
