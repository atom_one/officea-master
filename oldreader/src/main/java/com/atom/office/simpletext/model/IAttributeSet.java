
package com.atom.office.simpletext.model;

/**
 * 属性集接口
 * <p>
 * 属性以ID，Value方式记录，ID类型是short，value类型是int
 * <p>

 * <p>
 * 日期:            2011-12-28

 *

 */
public interface IAttributeSet {

    /**
     * 得到属性集ID
     */
    public int getID();

    /**
     * 添加属性
     *
     * @param attrID
     * @param value
     */
    public void setAttribute(short attrID, int value);

    /**
     * 删除属性
     *
     * @param attrID
     */
    public void removeAttribute(short attrID);

    /**
     * 得到属性
     *
     * @param attrID
     * @param value
     */
    public int getAttribute(short attrID);

    /**
     * 合并属性
     */
    public void mergeAttribute(IAttributeSet attr);

    /**
     *
     */
    public IAttributeSet clone();

    /**
     *
     */
    public void dispose();

}
