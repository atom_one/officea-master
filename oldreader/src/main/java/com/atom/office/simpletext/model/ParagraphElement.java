
package com.atom.office.simpletext.model;

/**
 * 段落元素

 * <p>
 * 日期:            2011-12-28
 * <p>
 * 负责人:          ljj8494


 */
public class ParagraphElement extends AbstractElement
{

    /**
     * 
     */
    public ParagraphElement()
    {   
        super();
        leaf = new ElementCollectionImpl(10);
    }
    
    /**
     * 
     *
     */
    public String getText(IDocument doc)
    {
        int count = leaf.size();
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < count; i++)
        {
            text.append(leaf.getElementForIndex(i).getText(null));
        }
        return text.toString();
    }
    
    /**
     * 
     */
    public void appendLeaf(LeafElement leafElem)
    {
        leaf.addElement(leafElem);
    }
    
    /**
     * 
     */
    public IElement getLeaf(long offset)
    {
        return leaf.getElement(offset);
    }
    
    /**
     * 得到指定index的Offset
     */
    public IElement getElementForIndex(int index)
    {
        return leaf.getElementForIndex(index);
    }
    
    /**
     *
     *
     */
    public void dispose()
    {
        super.dispose();
        if (leaf != null)
        {
            leaf.dispose();
            leaf = null;
        }
    }
    
    //
    private ElementCollectionImpl leaf;
    //
}
