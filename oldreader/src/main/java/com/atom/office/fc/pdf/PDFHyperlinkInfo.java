
package com.atom.office.fc.pdf;

import android.graphics.RectF;

/**
 * PDF document hyperlink information

 * <p>
 * 日期:            2012-9-19
 * <p>
 * 负责人:          ljj8494


 */
public class PDFHyperlinkInfo extends RectF
{

    /**
     * 
     * @param l
     * @param t
     * @param r
     * @param b
     * @param pageNumber
     */
    public PDFHyperlinkInfo(float l, float t, float r, float b, int pageNumber, String uri)
    {
        super(l, t, r, b);
        this.pageNumber = pageNumber;
        this.strURI = uri;
    }
    
    /**
     * 
     */
    public int getPageNumber()
    
    {
        return this.pageNumber;
    }
    
    /**
     * 
     */
    public String getURL()
    {
        return this.strURI;
    }
    
    // PDF page number
    private int pageNumber;
    
    private String strURI;
}
