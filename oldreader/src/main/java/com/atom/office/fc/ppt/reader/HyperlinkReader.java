
package com.atom.office.fc.ppt.reader;

import com.atom.office.common.hyperlink.Hyperlink;
import com.atom.office.fc.openxml4j.opc.PackagePart;
import com.atom.office.fc.openxml4j.opc.PackageRelationship;
import com.atom.office.fc.openxml4j.opc.PackageRelationshipCollection;
import com.atom.office.fc.openxml4j.opc.PackageRelationshipTypes;
import com.atom.office.system.IControl;

import java.util.Hashtable;
import java.util.Map;

/**
 * 解析  hyperlink

 * Read版本:       Read V1.0
 * <p>
 * 作者:           jhy1790
 * <p>
 * 日期:           2012-3-6
 * <p>
 * 负责人:         jhy1790


 */
public class HyperlinkReader
{
    private static HyperlinkReader hyperlink = new HyperlinkReader();
    
    /**
     * 
     */
    public static HyperlinkReader instance()
    {
        return hyperlink;
    }
    
    /**
     * 获取hyperlink
     */
    public void getHyperlinkList(IControl control, PackagePart packagePart) throws Exception
    {
        link = new Hashtable<String, Integer>();
        PackageRelationshipCollection hyperlinkRelCollection =
            packagePart.getRelationshipsByType(PackageRelationshipTypes.HYPERLINK_PART);
        for (PackageRelationship hyperlinkRel : hyperlinkRelCollection)
        {
            String id = hyperlinkRel.getId();
            if (getLinkIndex(id) < 0)
            {
                link.put(id, control.getSysKit().getHyperlinkManage().addHyperlink(hyperlinkRel.getTargetURI().toString(), Hyperlink.LINK_URL));
            }
        }
    }
    
    /**
     * get hyperlink index
     * @param id
     * @return
     */
    public int getLinkIndex(String id)
    {
        if (link != null && link.size() > 0)
        {
            Integer index = link.get(id);
            if (index != null)
            {
                return index;
            }
        }
        return -1;
    }
    
    /**
     * 
     */
    public void disposs()
    {
        if (link != null)
        {
            link.clear();
            link = null;
        }
    }
 
    // hyperlink id and index
    private Map<String, Integer> link;
}
