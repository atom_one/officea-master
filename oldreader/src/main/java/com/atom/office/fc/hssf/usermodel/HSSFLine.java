
package com.atom.office.fc.hssf.usermodel;

import com.atom.office.fc.ShapeKit;
import com.atom.office.fc.ddf.EscherContainerRecord;
import com.atom.office.ss.model.XLSModel.AWorkbook;

/**
 * TODO: 文件注释

 * Read版本:       Read V1.0
 * <p>
 * 作者:           jhy1790
 * <p>
 * 日期:           2013-4-1
 * <p>
 * 负责人:         jhy1790


 */
public class HSSFLine extends HSSFSimpleShape
{

    public HSSFLine(AWorkbook workbook, EscherContainerRecord escherContainer, 
           HSSFShape parent, HSSFAnchor anchor, int shapeType)
    {
        super(escherContainer, parent, anchor);
        setShapeType(shapeType);
        processLineWidth();
        processLine(escherContainer, workbook);
        processArrow(escherContainer);
        setAdjustmentValue(escherContainer);
        processRotationAndFlip(escherContainer);
    }
    
    public Float[] getAdjustmentValue()
    {
        return adjusts;
    }
    
    public void setAdjustmentValue(EscherContainerRecord escherContainer)
    {
        adjusts = ShapeKit.getAdjustmentValue(escherContainer);
    }
    
    private Float adjusts[];
}
