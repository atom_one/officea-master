
package com.atom.office.fc.hslf.model.textproperties;

/**
 * a kind of auto number data

 * Read版本:       Read V1.0
 * <p>
 * 作者:           jhy1790
 * <p>
 * 日期:           2012-7-19
 * <p>
 * 负责人:         jhy1790
 * <p>
 *

 */
public class AutoNumberTextProp {
    /**
     *
     */
    public AutoNumberTextProp() {

    }

    /**
     * @param numberingType
     * @param start
     */
    public AutoNumberTextProp(int numberingType, int start) {
        this.numberingType = numberingType;
        this.start = start;
    }

    /**
     * @return
     */
    public int getNumberingType() {
        return numberingType;
    }

    /**
     * @param numberingType
     */
    public void setNumberingType(int numberingType) {
        this.numberingType = numberingType;
    }

    /**
     * @return
     */
    public int getStart() {
        return start;
    }

    /**
     * @param start
     */
    public void setStart(int start) {
        this.start = start;
    }

    /**
     *
     */
    public void dispose() {

    }

    //
    private int numberingType = -1;
    //
    private int start = 0;
}
