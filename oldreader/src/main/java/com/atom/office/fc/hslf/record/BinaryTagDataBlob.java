
package com.atom.office.fc.hslf.record;

/**
 * TODO: An atom record that contains the value of the name-value pair 
 * in a programmable tag.

 * 日期:            2013-1-8
 * <p>
 *


 */
public class BinaryTagDataBlob extends PositionDependentRecordContainer
{
    private byte[] _header;
    public static long RECORD_ID = 0x138B;
    
    /**
     * We are of type 0xF144
     */
    public long getRecordType()
    {
        return RECORD_ID;
    }
    
    /**
     * Set things up, and find our more interesting children
     */
    protected BinaryTagDataBlob(byte[] source, int start, int len)
    {
        // Grab the header
        _header = new byte[8];
        System.arraycopy(source, start, _header, 0, 8);

        // Find our children
        _children = Record.findChildRecords(source, start + 8, len - 8);
    }
}

