
package com.atom.office.fc.hslf.record;

/**
 * TODO: A container record that specifies an effect behavior that transforms 
 * the image of an object. The transformation provides the ability to perform
 * transitions on objects. There is no property to be animated in this behavior. 
 * The behavior.stringList field is ignored.

 * 日期:            2013-1-7
 * <p>
 *


 */
public class TimeEffectBehaviorContainer extends PositionDependentRecordContainer
{
    private byte[] _header;
    public static long RECORD_ID = 0xF12D;
    
    /**
     * We are of type 0xF13D
     */
    public long getRecordType()
    {
        return RECORD_ID;
    }
    
    /**
     * Set things up, and find our more interesting children
     */
    protected TimeEffectBehaviorContainer(byte[] source, int start, int len)
    {
        // Grab the header
        _header = new byte[8];
        System.arraycopy(source, start, _header, 0, 8);

        // Find our children
        _children = Record.findChildRecords(source, start + 8, len - 8);
    }
}
