
package com.atom.office.fc.hslf.record;

import com.atom.office.fc.util.LittleEndian;

import java.util.Hashtable;

/**
 * TODO: An atom record that specifies the slide creation time stamp

 * 日期:            2013-1-6
 * <p>
 *


 */
public class SlideTimeAtom extends PositionDependentRecordAtom
{
    private byte[] _header;
    private static long _type = 12011;
    /**
     * the time of slide creation.
     */
    private long fileTime;
    
    /**
     * For the UserEdit Atom
     */
    protected SlideTimeAtom(byte[] source, int start, int len)
    {
        // Sanity Checking
        if(len < 16) { len = 16; }
        
        // Get the header
        _header = new byte[8];
        System.arraycopy(source, start, _header, 0, 8);
        
        fileTime = LittleEndian.getLong(source, start + 8);
    }

    /**
     * 
     * @return
     */
    public long getSlideCreateTime()
    {
        return fileTime;
    }
    
    /**
     * We are of type 12011
     */
    public long getRecordType()
    { 
        return _type; 
    }
    
    /**
     * At write-out time, update the references to PersistPtrs and
     *  other UserEditAtoms to point to their new positions
     */
    public void updateOtherRecordReferences(Hashtable<Integer,Integer> oldToNewReferencesLookup)
    {
       
    }
    
    /**
     * 
     */
    public void dispose()
    {
        _header = null;
    }
}
