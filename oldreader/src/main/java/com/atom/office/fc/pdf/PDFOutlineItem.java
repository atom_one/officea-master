
package com.atom.office.fc.pdf;

/**
 * PDF document outline information

 * <p>
 * 日期:            2012-9-19
 * <p>
 * 负责人:          ljj8494


 */
public class PDFOutlineItem
{
    /**
     * 
     * @param _level
     * @param _title
     * @param _page
     */
    public PDFOutlineItem(int _level, String _title, int pageNumber)
    {
        level = _level;
        title = _title;
        this.pageNumber = pageNumber;
    }
    
    
    /**
     * @return Returns the level.
     */
    public int getLevel()
    {
        return level;
    }


    /**
     * @return Returns the title.
     */
    public String getTitle()
    {
        return title;
    }


    /**
     * @return Returns the pageNumber.
     */
    public int getPageNumber()
    {
        return pageNumber;
    }


    //
    private final int level;
    private final String title;
    private final int pageNumber;
}
