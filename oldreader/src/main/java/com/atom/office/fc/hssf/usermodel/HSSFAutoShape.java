
package com.atom.office.fc.hssf.usermodel;

import com.atom.office.fc.ShapeKit;
import com.atom.office.fc.ddf.EscherContainerRecord;
import com.atom.office.ss.model.XLSModel.AWorkbook;

/**
 * autoshape data

 * Read版本:       Read V1.0
 * <p>
 * 作者:           jhy1790
 * <p>
 * 日期:           2013-3-27
 * <p>
 * 负责人:         jhy1790


 */
public class HSSFAutoShape extends HSSFTextbox
{   
    public HSSFAutoShape(AWorkbook workbook, EscherContainerRecord escherContainer, HSSFShape parent, 
           HSSFAnchor anchor, int shapeType)
    {
        super(escherContainer, parent, anchor);
        
        setShapeType(shapeType);
        processLineWidth();
        processLine(escherContainer, workbook);
        processSimpleBackground(escherContainer, workbook);
        processRotationAndFlip(escherContainer);
        
        //word art
    	String unicodeText = ShapeKit.getUnicodeGeoText(escherContainer);
    	if(unicodeText != null && unicodeText.length() > 0)
    	{
    		setString(new HSSFRichTextString(unicodeText));
    		setWordArt(true);
    		
    		setNoFill(true);
    		setFontColor(getFillColor());
    	}   
    } 

    
    public Float[] getAdjustmentValue()
    {
        return adjusts;
    }
    
    public void setAdjustmentValue(EscherContainerRecord escherContainer)
    {
        adjusts = ShapeKit.getAdjustmentValue(escherContainer);
    }    
    
    private Float adjusts[];
}
