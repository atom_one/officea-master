
package com.atom.office.fc.hslf.record;

/**
 * TODO: A container record that specifies a time condition of a time node.

 * 日期:            2013-1-7
 * <p>
 *


 */
public class TimeConditionContainer extends PositionDependentRecordContainer
{
    private byte[] _header;
    private static long _type = 0xF125;
    
    /**
     * We are of type 0xF13D
     */
    public long getRecordType()
    {
        return _type;
    }
    
    /**
     * Set things up, and find our more interesting children
     */
    protected TimeConditionContainer(byte[] source, int start, int len)
    {
        // Grab the header
        _header = new byte[8];
        System.arraycopy(source, start, _header, 0, 8);

        // Find our children
        _children = Record.findChildRecords(source, start + 8, len - 8);
    }
}
