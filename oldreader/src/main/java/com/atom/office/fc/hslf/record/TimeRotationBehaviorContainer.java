
package com.atom.office.fc.hslf.record;

/**
 * TODO: A container record that specifies a rotation behavior 
 * that rotates an object. This animation behavior is applied to the object 
 * specified by the behavior.clientVisualElement field and used to animate 
 * one property specified by the behavior.stringList field. 
 * The property MUST be "r" or "ppt_r" from the list that is specified 
 * in the TimeStringListContainer record.

 * 日期:            2013-1-7
 * <p>
 *


 */
public class TimeRotationBehaviorContainer extends PositionDependentRecordContainer
{
    private byte[] _header;
    public static long RECORD_ID = 0xF12F;
    
    /**
     * We are of type 0xF13D
     */
    public long getRecordType()
    {
        return RECORD_ID;
    }
    
    /**
     * Set things up, and find our more interesting children
     */
    protected TimeRotationBehaviorContainer(byte[] source, int start, int len)
    {
        // Grab the header
        _header = new byte[8];
        System.arraycopy(source, start, _header, 0, 8);

        // Find our children
        _children = Record.findChildRecords(source, start + 8, len - 8);
    }
}
