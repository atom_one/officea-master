
package com.atom.office.fc.ppt;

import android.graphics.Bitmap;

import com.atom.office.constant.MainConstant;

/**
 * get thumbnail of PPT document

 * <p>
 * 日期:            2012-12-13
 * <p>
 * 负责人:          ljj8494


 */
public class PPTReaderThumbnail
{
    //
    private static PPTReaderThumbnail kit = new PPTReaderThumbnail();
    
    public static  PPTReaderThumbnail instance()
    {
        return kit;
    }
    
    /**
     * 
     */
    public Bitmap getThumbnail(String filePath)
    {
        try
        {
            String fileName = filePath.toLowerCase();
            // ppt
            if (fileName.endsWith(MainConstant.FILE_TYPE_PPT)
                     || fileName.endsWith(MainConstant.FILE_TYPE_POT))
            {
                return getThumbnailForPPT(filePath);
            }
            // pptx
            else if (fileName.endsWith(MainConstant.FILE_TYPE_PPTX)
                     || fileName.endsWith(MainConstant.FILE_TYPE_PPTM)
                     || fileName.endsWith(MainConstant.FILE_TYPE_POTX)
                     || fileName.endsWith(MainConstant.FILE_TYPE_POTM))
            {
                return getThumbnailForPPT(filePath);
            }
        }
        catch (Exception e)
        {
            return null;
        }
        return null;
    }
    
    /**
     * 
     * @param filePath
     * @return
     * @throws Exception
     */
    private Bitmap getThumbnailForPPT(String filePath) throws Exception
    {
        
        return null;
    }
    
    /**
     * 
     * @param file
     * @return
     */
    private Bitmap getThumbnailForPPTX(String filePath) throws Exception
    {
        return null;
    }
}
